// SAMPLE
this.manifest = {
    "name": "Splunk Query Finder Options",
    "icon": "splunk-logo-32.png",
    "settings": [
        {
            "tab": "Link repositories",
            "group": "How does it work?",
            "name": "myDescription",
            "type": "description",
            "text": "You can add up to 5 websites which will be scanned for <pre>&lt;a href='link'&gt;Link name&lt;/a&gt;</pre> elements containing links in format: " +
                "<pre>https://splunk.paas-inf.net/.*/search/search?q=.*</pre>"
        },
        {
            "tab": "Link repositories",
            "group": "Repository name 1",
            "name": "website-name1",
            "type": "text",
            "text": "ex. Fortress Splunk Archive"
        },
        {
            "tab": "Link repositories",
            "group": "Website URL 1",
            "name": "website-url1",
            "type": "text",
            "text": "ex. https://hello.atlassian.net/wiki/spaces/I/pages/163859156/Fortress+splunk+archive"
        },
        {
            "tab": "Link repositories",
            "group": "Repository name 2",
            "name": "website-name2",
            "type": "text",
            "text": "ex. Fortress Splunk Archive"
        },
        {
            "tab": "Link repositories",
            "group": "Website URL 2",
            "name": "website-url2",
            "type": "text",
            "text": "ex. https://hello.atlassian.net/wiki/spaces/I/pages/163859156/Fortress+splunk+archive"
        },
        {
            "tab": "Link repositories",
            "group": "Repository name 3",
            "name": "website-name3",
            "type": "text",
            "text": "ex. Fortress Splunk Archive"
        },
        {
            "tab": "Link repositories",
            "group": "Website URL 3",
            "name": "website-url3",
            "type": "text",
            "text": "ex. https://hello.atlassian.net/wiki/spaces/I/pages/163859156/Fortress+splunk+archive"
        },
        {
            "tab": "Link repositories",
            "group": "Repository name 4",
            "name": "website-name4",
            "type": "text",
            "text": "ex. Fortress Splunk Archive"
        },
        {
            "tab": "Link repositories",
            "group": "Website URL 4",
            "name": "website-url4",
            "type": "text",
            "text": "ex. https://hello.atlassian.net/wiki/spaces/I/pages/163859156/Fortress+splunk+archive"
        },
        {
            "tab": "Link repositories",
            "group": "Repository name 5",
            "name": "website-name5",
            "type": "text",
            "text": "ex. Fortress Splunk Archive"
        },
        {
            "tab": "Link repositories",
            "group": "Website URL 5",
            "name": "website-url5",
            "type": "text",
            "text": "ex. https://hello.atlassian.net/wiki/spaces/I/pages/163859156/Fortress+splunk+archive"
        },
        {
            "tab": "Browser history",
            "group": "Saerch period",
            "name": "history_days",
            "type": "slider",
            "label": "How many days back of browser history search through:",
            "max": 30,
            "min": 1,
            "step": 1,
            "display": true,
            "displayModifier": function (value) {
                return value + " days";
            }
        },
    ]
};
