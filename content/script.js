var s = document.createElement('script');
s.src = chrome.extension.getURL('lib/utils.js');
s.onload = function () {
    this.remove();
};
(document.head || document.documentElement).appendChild(s);

var s = document.createElement('script');
s.src = chrome.extension.getURL('lib/mousetrap.min.js');
s.onload = function () {
    this.remove();
};
(document.head || document.documentElement).appendChild(s);

var s = document.createElement('script');
s.src = chrome.extension.getURL('js/injected.js');
s.onload = function () {
    this.remove();
};
(document.head || document.documentElement).appendChild(s);

$('head').append('<link rel="stylesheet" href="' + chrome.extension.getURL('css/style.css') + '" type="text/css" />');

(function (history) {
    var pushState = history.pushState;
    history.pushState = function (state) {
        if (typeof history.onpushstate == "function") {
            history.onpushstate({state: state});
        }
        return pushState.apply(history, arguments);
    }
})(window.history);

function queryForPages(data, pages) {
    for (var i = 1; i <= 5; i++) {
        try {
            let name = data["store.settings.website-name" + i].replace(/"/g, "");
            let url = data["store.settings.website-url" + i].replace(/"/g, "");
            if (name && url && name !== "" && url !== "") {
                pages.push({name: name, url: url, dataUrl: url});
            }
        } catch (t) {
            //don't log it
        }
    }
}

function queryForWebLinks(pages) {
    pages.forEach(page => {
        var url = page.dataUrl
        var found = url.match(/pages\/(\d+)\//i);
        console.log(found);
        if (found[1]) {
            page.dataUrl = "https://hello.atlassian.net/wiki/plugins/viewsource/viewpagesrc.action?pageId=" + found[1];
        }
        chrome.runtime.sendMessage(
            {
                contentScriptQuery: "queryConfluence",
                source: page.name,
                dataUrl: page.dataUrl,
                url: page.url,
            },
            function (response) {
                console.log(response.data.length + " website: " + page.name + " Splunk links loaded");
                $(response.data).each(function (i, item) {
                    addQuery(item.title, item.splunkQuery, item.url, item.source);
                });
            }
        )
    })
}

$(function () {

    $("body").append('<div id="query-finder-logo-url" style="display: none" url="' + chrome.extension.getURL('images/splunk-logo-32.png') + '"></div>')

    let pages = [];
    let historyDays = 7;

    chrome.runtime.sendMessage({action: "getConfiguration"}, function (response) {
        var data = response.data;
        if (data["store.settings.history_days"]) {
            historyDays = data["store.settings.history_days"];
            console.log("Found configuration for history with " + historyDays);
        }
        console.log(data);
        queryForPages(data, pages);
        queryForWebLinks(pages);

    });

    chrome.runtime.sendMessage({action: "getSplunkHistory", historyDays: historyDays}, function (response) {
        console.log(response.data.length + " history Splunk links loaded");
        $(response.data).each(function (i, item) {
            addQuery(item.title, item.splunkQuery, item.url, item.source);
        });
    });

    chrome.runtime.sendMessage({action: "getSplunkBookmarks"}, function (response) {
        console.log(response.data.length + " bookmarks Splunk links loaded");
        $(response.data).each(function (i, item) {
            addQuery(item.title, item.splunkQuery, item.url, item.source);
        });
    });



});

function addQuery(title, query, url, source) {

    let sourceSpan = '<span style="float: right; font-size: 10px; color: #b1b1b1">' + source + '</span>'
    let div = $("<div/>").append('<h3>' + title + ' - <a href="' + url + '">Splunk link</a></h3>', query);
    $(div).find("h3").append(sourceSpan);
    $("#confluence_links").append(div);
}

