function processNode(node) {
    let bookmarks = [];
    // recursively process child nodes
    if (node.children) {
        node.children.forEach(function (child) {
            bookmarks = bookmarks.concat(processNode(child));
        });
    }
    // print leaf nodes URLs to console
    if (node.url && node.url.includes("splunk.paas-inf.net")) {
        let bookmark = {};
        bookmark.url = node.url;
        bookmark.title = node.title;
        bookmarks.push(bookmark);
    }
    return bookmarks;
}

function createBookmark(url, title, source) {
    let regex = /q=(.[^&]*)&/g;
    let match = regex.exec(url);
    if (match != null && match.length > 1) {
        let bookmark = {};
        bookmark.url = url;
        bookmark.title = title;
        bookmark.splunkQuery = decodeURIComponent(match[1]).trim();
        bookmark.source = source;
        return bookmark;
    }
}
chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.action == "getConfiguration") {
            sendResponse({data:localStorage})
            return true;
        }
    });

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.action == "getSplunkBookmarks") {
            let bookmarks = [];
            chrome.bookmarks.getTree(function (itemTree) {
                itemTree.forEach(function (item) {
                    bookmarks = bookmarks.concat(processNode(item));
                });
                sendResponse({data: bookmarks.map(el => createBookmark(el.url, el.title, "Bookmarks")).filter(e => e != null)});
            });
            return true;
        }
    });

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.action == "getSplunkHistory") {
            let historyDays = request.historyDays;
            let bookmarks = [];
            let queries = [];
            var start = new Date();
            console.log("History days: " + historyDays)
            start.setHours(start.getHours() - 24 * historyDays);
            chrome.history.search({
                text: "splunk.paas-inf.net/en-US/app/search/search",
                maxResults: 1000,
                startTime: start.getTime()
            }, function (results) {
                console.log("Found " + results.length + " browser history results")
                results.forEach(result => {
                let bookmark = createBookmark(result.url, result.title, "Browser history")
                    if (bookmark != null && bookmark.splunkQuery != null && queries.indexOf(bookmark.splunkQuery) == -1) {
                        bookmarks.push(bookmark);
                        queries.push(bookmark.splunkQuery);
                    }
                });
                sendResponse({data: bookmarks});
            });
            return true;
        }
    });


chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.contentScriptQuery == "queryConfluence") {
            let url = request.dataUrl;
            console.log("Fetching: " + url);
            let bookmarks = [];
            fetch(url)
                .then(response => response.text())
                .then(text =>  {
                    $(text).find("a[href^='https://splunk.paas-inf.net'][href*='search/search?']")
                        .each(function (i, item) {
                        let url = $(item).attr("href");
                        let title = $(item).text();
                        bookmarks.push(createBookmark(url, title, request.source));
                    });
                    console.log(bookmarks);
                    sendResponse({data: bookmarks});
                })
        }
        return true;
    });

