$.expr[":"].contains = $.expr.createPseudo(function(arg) {
    return function( elem ) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

function waitUntilExists(selector, callback) {
    let interval = setInterval(function () {
        if ($(selector).length) {
            clearInterval(interval);
            callback();
        }
    }, 100);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}
